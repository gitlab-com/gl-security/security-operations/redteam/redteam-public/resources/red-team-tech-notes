These are raw notes as I go through various documentation, studies, etc. Eventually might incorporate into the main section

## Attack


- G Suite
    - Initial access
        - User-driven password recovery via phone or email (off by default for normal users, ON by default for admins https://support.google.com/a/answer/3033063)
        - Admin-driven password reset (via social engineering)
        - Shared calendars (also entry to Zoom!)
    - Persistence
    - Discovery
        - Calendar resources
    - Lateral movement
        - GMail delegation
    - Collection
        - Offline Gmail access? (is this applicable?)
        - Admin - restore deleted user files (https://support.google.com/a/answer/6052340)
    - Exfiltration
        - External contacts
        - Directory (include custom directories)
        
- Zoom
    - Initial access
        - Personal meeting links (find one, enum others)
    
## Threat Model

- G Suite
    - Entry Points
        - Create / Modify Accounts
            - Google Cloud Directory Sync
                - What is the bit about base64 and/or plain text password sync?!
                - It saves some XML files? Are they sensitive?
            - GSuite Password sync
            - Admin SDK - Directory API
        - Marketplace Apps
    - Data to examine
        - Calendar resources (buildings, conf rooms, etc)
        
## Notes

### G Suite

- Org Structure - single organization by default, can create OUs below that.
- Each account belongs only to a single OU
- In addition to OU, "access groups" can be used to more granularly enable (not disable) services. While groups can enable a service blocked by the OU, they cannot disable a service allowed by the OU.
- The directory contains lots of OSINT info and can be shared 
- Directory sharing affects what third-party applications and users outside your organization can see
    - External Directory Sharing is enabled by default and has two layers
- Contact sharing affects what users inside your organization can see
    - Enabled by default
    - Configurable whether or not to share cross-domain if you manage multiple domains
- Custom directories can limit visibility inside one org (restrict auto-complete, lists, etc). You assign a custom directory to an OU (they can see it), and choose to include GROUPS that can be seen
- External contacts may be a good source of OSINT
- Employee IDs can be used as a login challenge (https://support.google.com/a/answer/9022736)
- Administrator roles:
    - pre-built (available on home page under "admin roles")
    - custom
- Do a blurb on using the Google API explorer for building curl requests
- API to list all users in directory: https://developers.google.com/people/api/rest/v1/people/listDirectoryPeople
            

# Things to look into:

## GSuite
- By default, google groups permit "External" users to "Contact Owners". Might be interesting.
- GSuite suspended accounts (permissions on files, can they be enumerated, etc). Same with deleted accounts (20 day recovery period, transfer of ownership, etc)
- Why does a GSuite OU change possibly take 24 hours to complete? Any possible tomfoolery here?
- Look at API responses to the directory based on the "External Directory Sharing" settings
- Look at API to create external contacts - any tricks there?
- Install and test GC directory Sync
- Look into prebuilt and custom roles, explore ways to escalate privileges (example - can help desk reset a password of a user with higher priv?)
- Some neat looking permissions:
    - "Manage uploading private applications to the managed Google Play store"
    - "Manage uploading private applications with APKs hosted outside of Google Play"
- Investigate the implications of rapid release track: https://workspace.google.com/whatsnew/calendar/
- Go through the custom settings for every core app, look for abuse scenarios
- Look into GSSMO (https://support.google.com/a/users/topic/23333)
- Look into drive & doc templates (admin -> apps -> drive -> templates
- Update threat model to include the two separate google drive sync programs: https://support.google.com/a/answer/7491633?hl=en&ref_topic=7455083

### Notes from https://www.youtube.com/watch?v=qdelpOxTXO4&feature=emb_logo
- Can enumerate if an email has a public calendar attached (calexe tool which uses hunter.io), watch for rate limiting
- Google dork (intext:google.com/a/company.com) for public google doc links, also search in git logs etc
- Google sites:
    - old: sites.google.com/a/uber.com
    - new: sites.google.com/uber.com/(enumerate this) - don't have to be logged in to brute


# GAM Notes
- look at bashrc alias for GAM


# Calendar notes



# Resources
- https://www.slideshare.net/dafthack/ok-google-how-do-i-red-team-gsuite
- https://www.youtube.com/watch?v=qdelpOxTXO4&amp;feature=emb_logo
- https://developers.google.com/admin-sdk/directory/v1/get-start/getting-started
- https://github.com/jay0lee/GAM
- https://groups.google.com/g/google-apps-manager?pli=1


