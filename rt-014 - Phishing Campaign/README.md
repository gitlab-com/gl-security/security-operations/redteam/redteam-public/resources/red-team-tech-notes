# Red Team Attack Operation RT-014 - Phishing - Expensify Template + Replay RT011

<table>
  <tr>
    <th colspan="2"><b>Attack Operation RT014 - Phishing - Expensify Template + RT011 Replay</b></th>
  </tr>
  <tr>
    <td><b>Adversary</b></td>
    <td>Attacker targeting GitLab employees with the goal of obtaining credentials</td>
  </tr>
  <tr>
    <td><b>Scope</b></td>
    <td>All GitLab team members</td>
    </td>
  </tr>
  <tr>
    <td><b>Goals</b></td>
    <td>
        - Trick a sampling of GitLab team members into exposing their GitLab.com credential<br>
        - Replay previous phishing attack <br>
        - Do not actually capture passwords - username only<br>
        - Increase awareness of phishing techniques and how to identify a potential phish</br>
    </td>
  </tr>
  <tr>
    <td><b>Outcomes</b></td>
    <td>
        - A random sampling of 300 GitLab team members was selected<br>
        - Expensify Template - 12/300 (4%) of targets clicked on the phishing link in the email<br>
        - Expensify Template - 5/300 (1.7%) exposed their GitLab.com credential<br>
        - Expensify Template - 13/300 (4.3%) reported the email as suspicious to SecOps<br>
        - RT011 Template - 20/300 (6.7%) of targets clicked on the phishing link in the email<br>
        - RT011 Template - 2/300 (.67%) of targets exposed their GitLab.com credential<br>
        - RT011 Template - 15/300 (5%) reported teh email as suspicious to SecOps</br>
    </td>
  </tr>
</table>

## Overview
The intent of this exercise was to emulate a targeted phishing (aka spear phishing) campaign against GitLab team members with the intent of capturing GitLab.com credentials. For this exercise, additional defences such as multi-factor authentication were not considered or part of the test. This phishing exercise would be considered a basic phishing attack concentrating on primary authentication credentials via a fake login page.

For this exercise Red Team obtained the domain name gitiab.com and configured it to use a third party email provider to facilitiate the delivery of the phishing email. The domain name and accompanying GSuite services were setup following best practices such as configuring email to use DKIM and obtaining legitimate SSL certificates. This helped the domain look less suspicious to automted phishing site detection and to human inspection. This legitimate looking infrastructure can be setup by an attacker very cheaply and in some cases for free. 

The phishing framework we leveraged for this exercise is an open source project known as GoPhish. We hosted the tool on a small linux system in our GCP infrastructure. GoPhish provides a flexible framework that is highly customizable and has built in capabilities to track and capture responses to phishing campaigns. 

## Attack Narrative
This attack narrative focuses only on the new template used and will not outline the previous template that was also sent. Please review [RT-011](https://gitlab.com/gitlab-com/gl-security/security-operations/gl-redteam/red-team-tech-notes/-/tree/master/RT-011%20-%20Phishing%20Campaign) for specifics on the previous template. 

For this exercise we randomly selected 300 GitLab team members as targets of the phishing email. The email, screenshot below, was designed to appear to be a legitimate Expensify offer from GitLab's IT department. Targets were asked to click on a link in order to accept their corporate card and this link was instead a fake GitLab.com login page hosted on the domain "gitiab.com". While an attacker would be able to easily capture both the username and password entered into the fake site, the Red Team determined that only capturing email addresses or login names was necessary for this exercise.

![Graphs](./data/RT014_Graph.png)

Of the 300 phishing emails delivered 12 recipients cicked on the provided link. Of those 12 recipients, 5 of them attempted to authenticate to the fake site. Only 13 recipients reported the suspicious email to SecOps. Those that entered their credentials were then redirected to the [GitLab Handbook](https://about.gitlab.com/handbook/security/#phishing-tests).

### Phishing Email & Website Samples

![Phishing Email](./data/phishingemail.png)

![EmailDetails1](./data/phishheader.png)

Targets could have identified that this was in fact a phishing email the following ways:

  - The email address was it-ops@gitiab.com - not a legitimate gitlab.com one. Similar-sounding domain names are a common technique used in targeted phishing campaigns.
  - The email references an older model of Macbook Pro than what most users already have. Subtle factual errors are often indicators of an illegitimate source.
  - No secondary communication method, such as Slack or a company call, provided an announcement regarding any laptop upgrades.
  - Email message header details in Gmail can be viewed (Open the message, then go to the More option in the upper right, then choose the Show Original option) to give specific clues as to the methods by which the email was generated.  Keywords such as "phish" and multiple references to the illegitimate top level domain gitlab.company are key indicators.


![Phishing Site](./data/RedTeamPhishingSiteMay2020.png)

The website could have been considered suspicious based on the following  
  - Once the link is clicked the URL displayed in the browser bar is https://gitiab.com/xxxxxx.
  - The request to login should also be considered suspicious if the target is already logged into GitLab.com.
  - Some users may have noticed a change to their usual method of authenticating using Okta's SSO.


## Recommendations
Users should be encouraged to review the following handbook entry - [GitLab Handbook](https://about.gitlab.com/handbook/security/#phishing-tests).

Due to the low number of reports of this phish Security Team should communicate to all GitLab team members on a more frequent basis about phishing attacks and what to do if one is suspected.

On a quarterly basis additional phishing exercises should be conducted targeting a different sample group.


